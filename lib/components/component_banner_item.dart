import 'package:flutter/material.dart';

class ComponentBannerItem extends StatelessWidget {
  const ComponentBannerItem({super.key, required this.imgUrl});

  final String imgUrl;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Image.network(
          imgUrl,
          fit: BoxFit.cover,
          width: MediaQuery.of(context).size.width,
        ),
      ),
    );
  }
}