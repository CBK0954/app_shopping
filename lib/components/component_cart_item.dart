import 'package:app_shopping/model/cart_detail_item.dart';
import 'package:flutter/material.dart';

class ComponentCartItem extends StatelessWidget {
  const ComponentCartItem({super.key, required this.cartDetailItem, required this.voidCallback});

  final CartDetailItem cartDetailItem;
  final VoidCallback voidCallback;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Image.network(
            cartDetailItem.thumbImageUrl,
            width: 100,
            height: 100,
            fit: BoxFit.fill,
          ),
          Column(
            children: [
              Text(cartDetailItem.goodsName),
              Text('정상가 : ${cartDetailItem.originPrice}'),
              Text('할인금액 : ${cartDetailItem.salePrice}'),
              Text('상품가격 : ${cartDetailItem.originPrice - cartDetailItem.salePrice}')
            ],
          ),
          Container(
            child: OutlinedButton(
              onPressed: voidCallback,
              child: Text('삭제'),
            ),
          )
        ],
      ),
    );
  }
}
