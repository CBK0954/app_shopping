import 'package:app_shopping/model/cart_detail_item.dart';

class CartListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<CartDetailItem> list;

  CartListResult(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory CartListResult.fromJson(Map<String, dynamic> json) {
    return CartListResult(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => CartDetailItem.fromJson(e)).toList()
    );
  }
}