class CartDetailItem {
  int cartId;
  String goodsCategory;
  String thumbImageUrl;
  String goodsName;
  double originPrice;
  double salePrice;

  CartDetailItem(this.cartId, this.goodsCategory, this.thumbImageUrl, this.goodsName, this.originPrice, this.salePrice);

  factory CartDetailItem.fromJson(Map<String, dynamic> json) {
    return CartDetailItem(
      json['cartId'],
      json['goodsCategory'],
      json['thumbImageUrl'],
      json['goodsName'],
      json['originPrice'],
      json['salePrice'],
    );
  }
}