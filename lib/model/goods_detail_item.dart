class GoodsDetailItem {
  int id;
  String goodsCategory;
  String thumbImageUrl;
  String goodsName;
  double originPrice;
  double salePrice;
  double resultPrice;

  GoodsDetailItem(this.id, this.goodsCategory, this.thumbImageUrl, this.goodsName, this.originPrice, this.salePrice, this.resultPrice);

  factory GoodsDetailItem.fromJson(Map<String, dynamic> json) {
    return GoodsDetailItem(
      json['id'],
      json['goodsCategory'],
      json['thumbImageUrl'],
      json['goodsName'],
      json['originPrice'],
      json['salePrice'],
      json['resultPrice'],
    );
  }
}