class LoginResponse {
  int id;
  String name;

  LoginResponse(this.id, this.name);

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
        json['id'],
        json['name']
    );
  }
}
