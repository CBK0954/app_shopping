class BannerListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<String> list;

  BannerListResult(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory BannerListResult.fromJson(Map<String, dynamic> json) {
    return BannerListResult(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => e as String).toList()
    );
  }
}