class CartCreateRequest {
  int memberId;
  int goodsId;

  CartCreateRequest(this.memberId, this.goodsId);

  Map<String, dynamic> toJson() {
    Map<String, dynamic> result = Map<String, dynamic>();

    result['memberId'] = this.memberId;
    result['goodsId'] = this.goodsId;

    return result;
  }
}