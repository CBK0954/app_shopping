import 'package:app_shopping/model/goods_detail_item.dart';

class GoodsListResult {
  bool isSuccess;
  int code;
  String msg;
  int totalItemCount;
  int totalPage;
  int currentPage;
  List<GoodsDetailItem> list;

  GoodsListResult(this.isSuccess, this.code, this.msg, this.totalItemCount, this.totalPage, this.currentPage, this.list);

  factory GoodsListResult.fromJson(Map<String, dynamic> json) {
    return GoodsListResult(
        json['isSuccess'] as bool,
        json['code'],
        json['msg'],
        json['totalItemCount'],
        json['totalPage'],
        json['currentPage'],
        json['list'] == null ? [] : (json['list'] as List).map((e) => GoodsDetailItem.fromJson(e)).toList()
    );
  }
}