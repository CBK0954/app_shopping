import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/goods_list_result.dart';
import 'package:dio/dio.dart';

class RepoGoods {
  Future<GoodsListResult> getList(String goodsCategory) async {
    const String baseUrl = '$apiUri/goods/all/category';

    Map<String, dynamic> params = {};
    params['goodsCategory'] = goodsCategory;

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        queryParameters: params,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return GoodsListResult.fromJson(response.data);
  }
}