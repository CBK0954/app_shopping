import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/cart_create_request.dart';
import 'package:app_shopping/model/cart_list_result.dart';
import 'package:app_shopping/model/common_response.dart';
import 'package:dio/dio.dart';

class RepoCart {
  Future<CommonResponse> setData(int goodsId) async {
    const String baseUrl = '$apiUri/cart/goods';
    
    CartCreateRequest request = CartCreateRequest(1, goodsId);

    Dio dio = Dio();

    final response = await dio.post(baseUrl,
        data: request.toJson(),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }));

    return CommonResponse.fromJson(response.data);
  }

  Future<CartListResult> getList() async {
    const String baseUrl = '$apiUri/cart/list/my/member-id/1';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CartListResult.fromJson(response.data);
  }

  Future<CommonResponse> delData(int cartId) async {
    const String baseUrl = '$apiUri/cart/cart-id/{cartId}';

    Dio dio = Dio();

    final response = await dio.delete(
        baseUrl.replaceAll('{cartId}', cartId.toString()),
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return CommonResponse.fromJson(response.data);
  }
}