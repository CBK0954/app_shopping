import 'package:app_shopping/config/config_api.dart';
import 'package:app_shopping/model/banner_list_result.dart';
import 'package:dio/dio.dart';

class RepoBanner {
  Future<BannerListResult> getList() async {
    const String baseUrl = '$apiUri/banner/all/use';

    Dio dio = Dio();

    final response = await dio.get(
        baseUrl,
        options: Options(
            followRedirects: false,
            validateStatus: (status) {
              return status == 200;
            }
        )
    );

    return BannerListResult.fromJson(response.data);
  }
}