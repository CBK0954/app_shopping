import 'package:app_shopping/components/common/component_custom_loading.dart';
import 'package:app_shopping/components/common/component_notification.dart';
import 'package:app_shopping/components/component_banner_item.dart';
import 'package:app_shopping/components/component_goods_item.dart';
import 'package:app_shopping/model/goods_detail_item.dart';
import 'package:app_shopping/repository/repo_banner.dart';
import 'package:app_shopping/repository/repo_cart.dart';
import 'package:app_shopping/repository/repo_goods.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';

class PageMain extends StatefulWidget {
  const PageMain({Key? key}) : super(key: key);

  @override
  State<PageMain> createState() => _PageMainState();
}

class _PageMainState extends State<PageMain> {
  List<String> imgList = [];

  List<GoodsDetailItem> goodsListEat = [];

  Future<void> _getBannerList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoBanner().getList().then((res) {
      setState(() {
        imgList = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  Future<void> _setCart(int goodsId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoCart().setData(goodsId).then((res) {
      ComponentNotification(
        success: true,
        title: '장바구니 추가 완료',
        subTitle: '장바구니 추가 완료 되었습니다.',
      ).call();

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '장바구니 추가 실패',
        subTitle: '장바구니 추가에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  Future<void> _getGoodsList(String goodsCategory) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoGoods().getList(goodsCategory).then((res) {
      setState(() {
        goodsListEat = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _getBannerList();
    _getGoodsList('EAT');
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('choibk쇼핑')),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              child: CarouselSlider(
                options: CarouselOptions(
                  autoPlay: true,
                ),
                items: imgList.map((item) => ComponentBannerItem(imgUrl: item)).toList(),
              ),
            ),
            Text('식품1'),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for(int i = 0; i < goodsListEat.length; i++)
                    ComponentGoodsItem(
                      imgUrl: goodsListEat[i].thumbImageUrl,
                      goodsName: goodsListEat[i].goodsName,
                      goodsPrice: goodsListEat[i].resultPrice,
                      voidCallback: () {
                        _setCart(goodsListEat[i].id);
                      },
                    ),
                ],
              ),
            ),
            Text('식품2'),
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                children: [
                  for(int i = 0; i < goodsListEat.length; i++)
                    ComponentGoodsItem(
                      imgUrl: goodsListEat[i].thumbImageUrl,
                      goodsName: goodsListEat[i].goodsName,
                      goodsPrice: goodsListEat[i].resultPrice,
                      voidCallback: () {
                        _setCart(goodsListEat[i].id);
                      },
                    ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
