import 'package:app_shopping/functions/token_lib.dart';
import 'package:flutter/material.dart';

class PageMy extends StatefulWidget {
  const PageMy({Key? key}) : super(key: key);

  @override
  State<PageMy> createState() => _PageMyState();
}

class _PageMyState extends State<PageMy> {
  String? memberName;

  Future<void> _getMemberName() async {
    String? resultName = await TokenLib.getMemberName();
    setState(() {
      memberName = resultName;
    });
  }

  Future<void> _logout(BuildContext context) async {
    // functions/token_lib.dart 에서 만든 로그아웃 기능 호출
    TokenLib.logout(context);
  }

  @override
  void initState() {
    super.initState();
    _getMemberName();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
              'assets/harbor-night.jpg',
              width: MediaQuery.of(context).size.width,
              height: 300,
              fit: BoxFit.fill,
            ),
            Container(
              child: Text('$memberName님 즐거운 쇼핑되세요.'),
            ),
            OutlinedButton(
                onPressed: () {
                  _asyncConfirmDialog(context);
                },
                child: Text('로그아웃')
            ),
          ],
        ),
      ),
    );
  }

  Future<void> _asyncConfirmDialog(BuildContext context) async {
    return showDialog<void>(
      context: context,
      builder: (context) {
        return AlertDialog(
          title: Text('로그아웃'),
          content: Text('정말 로그아웃 하시겠습니까?'),
          actions: <Widget>[
            TextButton(
              child: Text('취소'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            TextButton(
              child: Text('확인'),
              onPressed: () {
                _logout(context);
                Navigator.of(context).pop();
              },
            )
          ],
        );
      },
    );
  }
}
