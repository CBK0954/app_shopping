import 'package:app_shopping/components/common/component_custom_loading.dart';
import 'package:app_shopping/components/common/component_notification.dart';
import 'package:app_shopping/components/component_cart_item.dart';
import 'package:app_shopping/model/cart_detail_item.dart';
import 'package:app_shopping/repository/repo_cart.dart';
import 'package:bot_toast/bot_toast.dart';
import 'package:flutter/material.dart';

class PageCart extends StatefulWidget {
  const PageCart({Key? key}) : super(key: key);

  @override
  State<PageCart> createState() => _PageCartState();
}

class _PageCartState extends State<PageCart> {
  List<CartDetailItem> cartItems = [];

  Future<void> _getCartList() async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoCart().getList().then((res) {
      setState(() {
        cartItems = res.list;
      });

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '데이터 로딩 실패',
        subTitle: '데이터 로딩에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  Future<void> _delCart(int cartId) async {
    BotToast.showCustomLoading(toastBuilder: (cancelFunc) {
      return ComponentCustomLoading(cancelFunc: cancelFunc);
    });

    await RepoCart().delData(cartId).then((res) {
      ComponentNotification(
        success: true,
        title: '장바구니 삭제',
        subTitle: '장바구니 삭제에 성공하였습니다.',
      ).call();

      _getCartList();

      BotToast.closeAllLoading();
    }).catchError((err) {
      ComponentNotification(
        success: false,
        title: '장바구니 삭제 실패',
        subTitle: '장바구니 삭제에 실패하였습니다.',
      ).call();


      BotToast.closeAllLoading();
    });
  }

  @override
  void initState() {
    super.initState();
    _getCartList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('장바구니'),
      ),
      body: ListView(
        children: [
          for (int i = 0; i < cartItems.length; i++)
            ComponentCartItem(
              cartDetailItem: cartItems[i],
              voidCallback: () {
                _delCart(cartItems[i].cartId);
              },
            ),
          OutlinedButton(
              onPressed: () {},
              child: Text('결제하기')
          ),
        ],
      ),
    );
  }
}
