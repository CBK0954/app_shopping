import 'package:app_shopping/pages/page_cart.dart';
import 'package:app_shopping/pages/page_main.dart';
import 'package:app_shopping/pages/page_my.dart';
import 'package:app_shopping/pages/page_sale.dart';
import 'package:flutter/material.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({Key? key}) : super(key: key);

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {
  int _selectedIndex = 0;

  final List<BottomNavigationBarItem> _navItems = [
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: '쇼핑홈',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.home),
      label: '핫세일',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.business),
      label: '장바구니',
    ),
    const BottomNavigationBarItem(
      icon: Icon(Icons.school),
      label: '마이페이지',
    ),
  ];

  final List<Widget> _widgetPages = [
    const PageMain(),
    const PageSale(),
    const PageCart(),
    const PageMy(),
  ];

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: _widgetPages.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: _navItems,
        currentIndex: _selectedIndex,
        onTap: _onItemTap,
        unselectedItemColor: Colors.black,
        selectedItemColor: Colors.amber[800],
      ),
    );
  }
}
